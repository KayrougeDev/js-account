function blog() {
    document.getElementById("blog-title").innerHTML = tr("Blog of",SESSION["lang"]) + " QL Software"
    
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            if(this.responseText == 'FAIL') {
                msg += '<div class="alert alert-danger text-center" role="alert">';
                msg += '<b>' + tr('There was a problem.<br />The change could not be performed.',SESSION["lang"]) + '</b>';
                msg += '</div>';
                document.getElementById('msg').innerHTML = msg;
            }
            else {
                const result = this.responseText.split("|");
                blogList = [];
                result.forEach(function(blog) {
                    if(blog != '') {
                        let blogDataList = blog.split("£");
                        let blogData = {
                            "id": blogDataList[0],
                            "uid": blogDataList[1],
                            "user": blogDataList[2],
                            "title": blogDataList[3],
                            "text": blogDataList[4],
                        };
                        blogList.push(blogData);
                        msg = "";
                        blogList.forEach(function(cBlog) {
                            msg += '<hr>';
                            msg += '<h2 style="font-size:30px;">'+cBlog["title"]+' <span style="font-size:25px;">'+tr("by",SESSION["lang"])+' <a class="link" href="#" target="blank">'+cBlog["user"]+'</a></span></h2>';
                            msg += '<h3 style="font-size:27.5px;">'+cBlog['text']+'</H3>';
                        });
                        document.getElementById('msg').innerHTML = msg;
                    }
                });
            }
        }
    }
    xmlhttp.open('POST', LOCAL_URL);
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send('what=loadBlog');
}

window.addEventListener('load', blog, false);
