/**
#-----------------------------------------------------------------
# The original file is a part of RDV project.
# Copyright:    (C) 2022 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# Used by KayrougeDev
#-----------------------------------------------------------------
**/

const APPNAME = 'CompteQL',
    LOCAL_SESSION_URL = "http://localhost/v2/",
    LOCAL_URL = LOCAL_SESSION_URL+'assets/script.php',
    PAGES_WITH_NAVBAR = ['profil','admin', 'password']

let SESSION = {
    'hostname': '',
    'user': '|-1|-1|||||',
    'userName': '',
    'lang': 'fr',
    'mustLoadData': true,
    'cards' : []
};

let PAGE = window.location.href.split('/');
PAGE = PAGE[PAGE.length - 1].split('.')[0];
if (PAGE == '')
    PAGE = 'index';

let PAGE_PATH = window.location.href.split('/');

function connect() {
        let login = document.getElementById('login').value;
        let password = document.getElementById('password').value;
        let url = LOCAL_URL;
        let xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                let user = this.responseText;
                SESSION['user'] = user;
                user = user.split('|');
                SESSION['userName']=user[4] + ' ' + user[3];
                if (user[1] > -1) {
                    saveSession();
                    window.location = LOCAL_SESSION_URL+'profil.html';
                }
                else if (user[2] > -1) {
                        saveSession();
                        window.location = LOCAL_SESSION_URL+'admin.html';
                }
                else {
                    let msg = '<p></p>';
                    msg += '<div class="alert alert-danger text-center" role="alert">';
                    msg += '<b>' + tr('Incorrect login or password!',SESSION["lang"]) + '</b>';
                    msg += '</div>';
                    document.getElementById('msg').innerHTML = msg;
                    }
                }
            };
        xmlhttp.open('POST', url);
        xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xmlhttp.send('what=login&login='+login+'&password=' + password);
        }

function checkIsConnected(redirect=false) {
    if(redirect) {
        if (PAGES_WITH_NAVBAR.indexOf(PAGE) != -1) {
            if(SESSION['user'].split("|")[1] == "-1") {
                if(SESSION['user'].split("|")[2] == "-1") {
                    window.location = LOCAL_SESSION_URL;
                }
                else {
                    if(PAGE == "profil")
                        window.location = LOCAL_SESSION_URL+"admin.html";
                }
            }
            else {
                if(PAGE == "admin")
                    window.location = LOCAL_SESSION_URL+"profil.html";
            }
        }
    }
    else {
        if((SESSION['user'].split("|")[1] == "-1") && (SESSION['user'].split("|")[2] == "-1"))
            return false;
    }

    return true;
}
        
function init() {
    if (localStorage) {
        let json = sessionStorage.getItem(APPNAME);
        if (json != null)
            SESSION = JSON.parse(json);
    }
    checkIsConnected(redirect=true);
    if(SESSION['mustLoadData']) {
        loadUserData();
    }
    updateNavBar();

    // footer (crédit, license)
    try {
        document.getElementById("footerCreditPage").innerHTML = tr('A part of the CSS and JS was created by: ',SESSION["lang"]);
        document.getElementById("footerLink").innerHTML = tr('edleh on project "RDV"',SESSION["lang"]);
        document.getElementById("footerCreditPage").innerHTML = tr('Credit',SESSION["lang"]);
    } catch {
    }
    

    // footer end

    if (PAGE == 'index') {
        // afficher-masquer le mot de passe :
        document.getElementById('btn-showPassword').onmouseover = function() {
            document.getElementById('password').type = 'text';
            };
        document.getElementById('btn-showPassword').onmouseout = function() {
            document.getElementById('password').type = 'password';
            };
        // bouton de connexion :
        document.getElementById('btn-start').addEventListener('click', connect, false);
        // touche entrée :
        document.getElementById('password').addEventListener('keyup', function(event) {
            if (event.keyCode === 13) {
                event.preventDefault();
                document.getElementById('btn-start').click();
                }
            });
        //if (SESSION['protocol'] == '')
        //    SESSION['protocol'] = window.location.protocol;
        if (SESSION['hostname'] == '')
            SESSION['hostname'] = window.location.hostname;
        // traductions :
        document.title = APPNAME + ' - ' + tr('Login',SESSION["lang"]);
        document.getElementById('login').setAttribute('placeholder', tr('Login',SESSION["lang"]));
        document.getElementById('password').setAttribute('placeholder', tr('Password',SESSION["lang"]));
        document.getElementById('btn-start').innerHTML = tr('Sign in',SESSION["lang"]);
        }
    else if (PAGE == 'profil') {
        document.title = APPNAME + ' - ' + tr('User',SESSION["lang"]) + " - "+SESSION["userName"];
        document.getElementById("btn-back").style.display = "none";
    }
    else if (PAGE == 'admin') {
        document.title = APPNAME + ' - ' + tr('Admin',SESSION["lang"]) + " - "+SESSION["userName"];
        document.getElementById("btn-back").style.display = "none";
        //loadCard();
    }
    else if (PAGE == 'credit') {
        document.title = APPNAME + ' - ' + tr('Credit',SESSION["lang"]);
    }
    else if (PAGE == 'password') {
        // afficher-masquer le mot de passe :
        document.getElementById('btn-showPassword-1').onmouseover = function() {
            document.getElementById('password-1').type = 'text';
            };
        document.getElementById('btn-showPassword-1').onmouseout = function() {
            document.getElementById('password-1').type = 'password';
            };
        document.getElementById('btn-showPassword-2').onmouseover = function() {
            document.getElementById('password-2').type = 'text';
            };
        document.getElementById('btn-showPassword-2').onmouseout = function() {
            document.getElementById('password-2').type = 'password';
            };

        // traductions :
        document.title = APPNAME + ' - ' + tr('password',SESSION["lang"]);
        document.getElementById('change-password').innerHTML = 
            tr('CHANGE PASSWORD',SESSION["lang"]);
        document.getElementById('text-1').innerHTML = tr('New password:',SESSION["lang"]);
        document.getElementById('text-2').innerHTML = tr('Confirmation:',SESSION["lang"]);

        document.getElementById('btn-save').innerHTML = tr('Save',SESSION["lang"]);
        document.getElementById('btn-save').setAttribute('title', tr('Save changes',SESSION["lang"]));
        document.getElementById('btn-save').addEventListener('click', save, false);
    }
    else if(PAGE == 'blog') {
        document.title = APPNAME + ' - Blog';
    }

}

function save() {
    if (PAGE == 'password') {
        let password1 = document.getElementById('password-1').value;
        let password2 = document.getElementById('password-2').value;
        let params = password1;
        let msg = '<p></p>';
        if (password1 != password2) {
            msg += '<div class="alert alert-danger text-center" role="alert">';
            msg += '<b>' + tr('The new password does not match the confirmation.',SESSION["lang"]) + '</b>';
            msg += '</div>'
            document.getElementById('msg').innerHTML = msg;
            }
        else {
            let xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    let result = this.responseText;
                    if (result == 'YES') {
                        msg += '<div class="alert alert-success text-center" role="alert">';
                        msg += '<b>' + tr('The password has been changed.',SESSION["lang"]) + '</b>';
                        document.getElementById('msg').innerHTML = msg;
                        }
                    else {
                        msg += '<div class="alert alert-danger text-center" role="alert">';
                        msg += '<b>' + tr('There was a problem.<br />The change could not be performed.',SESSION["lang"]) + '</b>';
                        document.getElementById('msg').innerHTML = msg;
                        }
                    }
                };
            xmlhttp.open('POST', LOCAL_URL);
            xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xmlhttp.send(
                'what=submit'
                + '&page=' + PAGE
                + '&user=' + SESSION['user']
                + '&params=' + params);
            }
    }
}

function saveSession() {
    if (localStorage) {
        let json = JSON.stringify(SESSION);
        sessionStorage.setItem(APPNAME, json);
    };
}

function destroySession() {
    if (localStorage) {
        sessionStorage.clear();
        document.location.href=LOCAL_SESSION_URL;
    };
}

function setLang(lang) {
    SESSION['lang'] = lang;
    saveSession();
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let result = this.responseText;
            if (result == 'NO') {
                msg += '<div class="alert alert-danger text-center" role="alert">';
                msg += '<b>' + tr('There was a problem.<br />The change could not be performed.',SESSION["lang"]) + '</b>';
                msg += '</div>';
                document.getElementById('msg').innerHTML = msg;
            }
        }
    };
    xmlhttp.open('POST', LOCAL_URL);
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send('what=updatePref&page=lang&user=' + SESSION["user"]+'&params='+SESSION['lang']);
    window.location = window.location.href;
}

function updateNavBar() {
    document.getElementById("btn-langDrop").innerHTML = tr('Language',SESSION["lang"]);

    if(PAGES_WITH_NAVBAR.indexOf(PAGE) == -1) {
        if(SESSION["userName"] == "") {
            document.getElementById('userName').innerHTML = "<a class='link' onClick='moveToMainPage();'>Non connecté</a>";
        }
        else {
            document.getElementById('userName').innerHTML = "<a class='link' onClick='moveToMainPage();'>"+SESSION['userName']+'</a>';
        }
    }

    if (PAGES_WITH_NAVBAR.indexOf(PAGE) != -1) {
        /*
        mise à jour des boutons de la barre de titre.
        On en profite pour gérer le bouton "Enregistrer"
        */

        document.getElementById('userName').innerHTML = SESSION['userName'];
        document.getElementById('btn-quit').setAttribute('title', tr('QUIT',SESSION["lang"]));
        document.getElementById('btn-quit').style.cursor = "pointer";
        document.getElementById('btn-quit').addEventListener('click',destroySession);

        document.getElementById('btn-back').setAttribute('title', tr('BACK',SESSION["lang"]));
        document.getElementById('btn-back').style.cursor = "pointer";

        let buttons = [
            [((SESSION["user"].split("|")[1] == "-1") ? "admin" : "profil"), 'Home', 'go-home'],
            ['password', 'Password', 'password'],
            ];
        let html = '';
        for (let i = 0; i < buttons.length; i++) {
            if (buttons[i][0] == PAGE)
                html += '<a class="navbar-brand disable" ';
            else
                html += '<a class="navbar-brand filter-white" ';
            html += 'href="' + buttons[i][0] + '.html" ';
            html += 'title="' + tr(buttons[i][1],SESSION["lang"]) + '">';
            html += '<img src="assets/images/' + buttons[i][2] + '.svg" width="24"></a>';
            }
        document.getElementById('navbar-buttons').innerHTML = html;
    }
}


window.addEventListener('load', init, false);

function loadCard() {
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            if(this.responseText == 'FAIL') {
                msg += '<div class="alert alert-danger text-center" role="alert">';
                msg += '<b>' + tr('There was a problem.<br />The change could not be performed.',SESSION["lang"]) + '</b>';
                msg += '</div>';
                document.getElementById('msg').innerHTML = msg;
            }
            else {
                const result = this.responseText.split("|");
                result.forEach(function(card) {
                    if(card != "") {
                        let cardDataList = card.split("£");
                        let cardData = {
                            "id": cardDataList[0],
                            "uid": cardDataList[1],
                            "title": cardDataList[2],
                            "recto": cardDataList[3],
                            "verso": cardDataList[4],
                        };
                        SESSION["cards"].push(cardData)
                        // SESSION["cards"].forEach(function(card) {
                        //     console.log(card)
                        //     let msg = " ";
                        //     msg += '<div class="alert alert-success text-center" role="alert">';
                        //     msg += '<b>' + card["id"] + " " + card["uid"] + " " + card["title"] + '</b>';
                        //     msg += '</div>';
                        //     document.getElementById('msg').innerHTML.push(msg)
                        // });
                    }
                });
            }
        }
    }
    xmlhttp.open('POST', LOCAL_URL);
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send('what=loadUserCard&user=' + SESSION['user']);
}

function loadUserData() {
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let result = this.responseText;
            let data = result.split("|");
            SESSION['lang'] = data[0];
            SESSION['mustLoadData'] = false;
            saveSession();
            window.location = window.location.href;
        }
    };
    xmlhttp.open('POST', LOCAL_URL);
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send('what=loadUserData&user=' + SESSION['user']);
}

function moveToMainPage() {
    if(SESSION['user'] == "|-1|-1|||||") {
        window.location = LOCAL_SESSION_URL;
    }
    else {
        if(SESSION['user'].split("|")[1] > 0) {
            window.location = LOCAL_SESSION_URL+"profil.html";
        }
        else {
            window.location = LOCAL_SESSION_URL+"admin.html";
        }
    }
}

function goBack() {
    window.history.back();
}

// bouton goTop
const goTop = document.getElementById('go-top');
window.addEventListener('scroll', function() {
    if (window.pageYOffset > 250)
        goTop.className = 'active';
    else
        goTop.className = '';
    });
