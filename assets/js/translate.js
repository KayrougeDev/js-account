/**
#-----------------------------------------------------------------
# The original file is a part of RDV project.
# Copyright:    (C) 2022 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# Used by KayrougeDev
#-----------------------------------------------------------------
**/


const TRANSLATIONS_ESP = {

    // Traducción realizada con DeepL.com

    // page index
    'login': 'conexión', 
    'Login': 'Identificador', 
    'Password': 'Contraseña', 
    'Sign in': 'Entrar en el sistema', 
    'Incorrect login or password!': 'Nombre de usuario o contraseña incorrecta.', 

    // page user
    'user': 'usuario', 
    'User': 'Usuario',
    'Administrator': 'Director',

    // page manager
    'Message': 'Mensaje', 
    //'Date': 'Date', 

    // page manage-users
    'users': 'usuarios',

    // page manage-message
    'message': 'mensaje', 
    'Changes saved.': 'Cambios registrados.',

    // page manage-edit
    'edit': 'editar',

    // page password
    'password': 'contraseña', 
    'CHANGE PASSWORD': 'CAMBIAR LA CONTRASEÑA', 
    'New password:': 'Nueva contraseña :', 
    'Confirmation:': 'Confirmación :', 
    'The new password does not match the confirmation.': 
        'La nueva contraseña no coincide con la confirmación.', 
    'The password has been changed.': 'La contraseña ha sido cambiada.', 
    'There was a problem.<br />The change could not be performed.': 
        "Hubo un problema.<br />El cambio no pudo realizarse.", 

    // utilisé dans plusieurs pages
    'Home': 'Inicio',
    'QUIT': 'SALIDA',
    'BACK': 'RETOUR',
    'Credit': 'Crédito',

    'Language': 'Idioma',

    'Blog of': 'Blog de',

    'Save': 'Registro', 
    'Save changes': 'Guardar los cambios', 

    'Unknow': 'Desconocido',
    'by': 'por',

    // footer credit
    'A part of the CSS and JS was created by:': 'Parte del CSS y JS fue creado por: ',
    'edleh on project "RDV"': 'edleh en el proyecto "RDV"',
    'Website': 'Página web',
    };

const TRANSLATIONS_FR = {

        // page index
        'login': 'connexion', 
        'Login': 'Identifiant', 
        'Password': 'Mot de passe', 
        'Sign in': 'Se connecter', 
        'Incorrect login or password!': 'Identifiant ou mot de passe incorrect !', 
    
        // page user
        'user': 'utilisateur', 
        'User': 'Utilisateur',
        'Administrator': "Administrateur",
    
        // page manager
        'Message': 'Message', 
        //'Date': 'Date', 
    
        // page manage-users
        'users': 'utilisateurs',
    
        // page manage-message
        'message': 'message', 
        'MESSAGE TO USERS': 'MESSAGE AUX UTILISATEURS', 
        'Changes saved.': 'Modifications enregistrées.',
    
        // page manage-edit
        'edit': 'modifier',
        'Student': 'Élève', 
    
        //'CONFIRMATION OF YOUR APPOINTMENT': 'CONFIRMATION DE VOTRE RENDEZ-VOUS', 
        'Student:': 'Élève :', 
    
        // page password
        'password': 'mot de passe', 
        'CHANGE PASSWORD': 'CHANGER DE MOT DE PASSE', 
        'New password:': 'Nouveau mot de passe :', 
        'Confirmation:': 'Confirmation :', 
        'The new password does not match the confirmation.': 
            'Le nouveau mot de passe ne correspond pas à la confirmation.', 
        'The password has been changed.': 'Le mot de passe a été changé.', 
        'There was a problem.<br />The change could not be performed.': 
            "Il y a eu un problème.<br />Le changement n'a pas pu être effectué.", 
    
        // utilisé dans plusieurs pages
        'Home': 'Accueil',
        'QUIT': 'QUITTER',
        'BACK': 'RETOUR',
        'Credit': 'Crédit',

        'Language': 'Langue',

        'Blog of': 'Blog de',
    
        'Save': 'Enregistrer', 
        'Save changes': 'Enregistrer les modifications', 

        'Unknow': 'Inconnue',
        'by': 'par',
    
        // Footer credit
        'Website': 'Site web',
        'edleh on project "RDV"': 'edleh sur le projet "RDV"',
        'A part of the CSS and JS was created by:':
            'Une partie du CSS et du JS a été créé par: '
        };

function tr(text,lang) {
    let result = "";
    if (lang == "esp")
        result = TRANSLATIONS_ESP[text.trim()];
    else if (lang == "en")
        return text
    else
        result = TRANSLATIONS_FR[text.trim()];
    if (result === undefined)
        result = text;
    else if (result == '')
        result = text;
    //console.log('TR : ' + text + ' -> ' + result);
    return result;
    }

