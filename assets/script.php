<?php

/*
#-----------------------------------------------------------------
# The original file is a part of RDV project.
# Copyright:    (C) 2022 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# Used by KayrougeDev
#-----------------------------------------------------------------
*/

$what = isset($_POST['what']) ? $_POST['what'] : '';
$login = isset($_POST['login']) ? $_POST['login'] : '';
$password = isset($_POST['password']) ? $_POST['password'] : '';
$user = isset($_POST['user']) ? $_POST['user'] : '|-1|-1||||';
$page = isset($_POST['page']) ? $_POST['page'] : '';
$params = isset($_POST['params']) ? $_POST['params'] : '';

function doEncode($texte, $algo='') {
    /*
    encode un texte en Sha1 ou Sha256 
    (pour mots de passe et logins)
    */
    if ($algo == 'sha256')
        return hash('sha256', $texte);
    elseif ($algo == 'sha1')
        return sha1($texte);
    else
        return [sha1($texte), hash('sha256', $texte)];
    }


function checkPassword($arg1,$arg2,$user_mode) {
    $pdo = new PDO('sqlite:'.dirname(__FILE__).'/db/users.sqlite');
    $pdo -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
    try {
        $pdo -> beginTransaction();

        $SQL = 'SELECT * FROM '.$user_mode.' WHERE Login=:login AND Mdp=:mdp';
        $OPT = array('login' => $arg1,'mdp' => $arg2);
        $STMT = $pdo -> prepare($SQL);
        $STMT -> execute($OPT);

        $data = $STMT->fetch();

        if($data == true) {
            return true;
        }
        else {
            return false;
        }

        }
    catch (PDOException $e) {
            $pdo -> rollBack();
            return false;
    }
}

function verifyLoginPassword(
        $login, $password, $user_mode) {
    /*
    vérifie les login et mot de passe proposés lors de la connexion.
    */
    $login_enc = doEncode($login);
    $password_enc = doEncode($password);
    $pdo = new PDO('sqlite:'.dirname(__FILE__).'/db/users.sqlite');
    $pdo -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
    $pdo -> beginTransaction();
    $SQL = 'SELECT * FROM '.$user_mode.'s ';
    $SQL .= 'WHERE Login IN (:login1, :login256) ';
    $SQL .= 'AND Mdp IN (:mdp1, :mdp256)';
    $OPT = array(
        ':login1' => $login_enc[0], ':login256' => $login_enc[1], 
        ':mdp1' => $password_enc[0], ':mdp256' => $password_enc[1]);
    $STMT = $pdo -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    return ($STMT != '') ? $STMT -> fetch() : NULL;
}

function getUserWithId($id,$user_mode) {
    $pdo = new PDO('sqlite:'.dirname(__FILE__).'/db/users.sqlite');
    $pdo -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
    try {
        $pdo -> beginTransaction();
        $STMT = $pdo -> prepare('SELECT * FROM '.$user_mode.' WHERE id=:id');
        $STMT -> setFetchMode(PDO::FETCH_ASSOC);
        $STMT -> execute(['id' => $id]);
        $data = $STMT->fetch();
        return $data;
    }
    catch (PDOException $e) {
            $pdo -> rollBack();
    }
}



if ($what == 'login') {
    $result = '|-1|-1||||||';
    $id_user = -1;
    $id_admin = -1;
    $NOM = '';
    $Prenom = '';
    $record = verifyLoginPassword($login, $password, 'user');
    if ($record) {
        $id_user = intval($record['id']);
        $NOM = $record['NOM'];
        $Prenom = $record['Prenom'];
        $result = '|'.$id_user.'|-1|'
            .$NOM.'|'.$Prenom.'|'
            .$record['Login'].'|'.$record['Mdp']
            .'|';
    }
    else {
        $record = verifyLoginPassword($login, $password, 'admin');
        if ($record) {
            $id_admin = intval($record['id']);
            $NOM = $record['NOM'];
            $Prenom = $record['Prenom'];
            $result = '|-1|'.$id_admin.'|'
                .$NOM.'|'.$Prenom.'|'
                .$record['Login'].'|'.$record['Mdp']
                .'|';
            }
        }

    }
    else if ($what == 'submit') {
        $result = 'NO';
        $user = explode('|', $user);

        if ($page == 'password') {
                $id_user = $user[1];
                $id_admin = $user[2];
                if ($id_admin < 0) {
                    $table = 'users';
                    $user_id = $id_user;
                    }
                else {
                    $table = 'admins';
                    $user_id = $id_admin;
                    }
                $mdp_enc = doEncode($params, $algo='sha256');

                if(checkPassword($user[5],$user[6],$table)) {
        
                    $pdo = new PDO('sqlite:'.dirname(__FILE__).'/db/users.sqlite');
                    $pdo -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
                    $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
                    try {
                        $pdo -> beginTransaction();
            
                        $SQL = 'UPDATE '.$table.' SET Mdp=:mdp WHERE id=:id';
                        $OPT = array(
                            ':id' => $user_id, 
                            ':mdp' => $mdp_enc);
                        $STMT = $pdo -> prepare($SQL);
                        $STMT -> execute($OPT);
            
                        $pdo -> commit();
                        $result = 'YES';
                        }
                    catch (PDOException $e) {
                            $pdo -> rollBack();
                        }
                    }
                }
            }
    else if ($what == "updatePref") {
        $result = 'NO';
        $user = explode('|', $user);
        if($page == "lang") {
            $id_user = $user[1];
            $id_admin = $user[2];
            if ($id_admin < 0) {
                $table = 'users';
                $user_id = $id_user;
            }
            else {
                $table = 'admins';
                $user_id = $id_admin;
            }

            if(checkPassword($user[5],$user[6],$table)) {

                $pdo = new PDO('sqlite:'.dirname(__FILE__).'/db/users.sqlite');
                $pdo -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
                $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
                try {
                    $pdo -> beginTransaction();
        
                    $SQL = 'UPDATE '.$table.' SET Lang=:lang WHERE id=:id';
                    $OPT = array(
                        ':id' => $user_id, 
                        ':lang' => $params);
                    $STMT = $pdo -> prepare($SQL);
                    $STMT -> execute($OPT);
        
                    $pdo -> commit();
                    $result = 'YES';
                    }
                catch (PDOException $e) {
                        $pdo -> rollBack();
                }
            }
        }
    }
    else if($what == "loadUserData") {
        $result = 'fr|';
        $user = explode('|', $user);
        $id_user = $user[1];
        $id_admin = $user[2];
        if ($id_admin < 0) {
            $table = 'users';
            $user_id = $id_user;
        }
        else {
            $table = 'admins';
            $user_id = $id_admin;
        }

        if(checkPassword($user[5],$user[6],$table)) {

            $pdo = new PDO('sqlite:'.dirname(__FILE__).'/db/users.sqlite');
            $pdo -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
            try {
                $pdo -> beginTransaction();

                $SQL = 'SELECT Lang FROM '.$table.' WHERE id=:id';
                $OPT = array(':id' => $user_id);
                $STMT = $pdo -> prepare($SQL);
                $STMT -> execute($OPT);

                $data = $STMT->fetch();

                $result = $data["Lang"]."|";
                }
            catch (PDOException $e) {
                    $pdo -> rollBack();
            }
        }
    }
    else if($what == "loadUserCard") {
        $result = 'FAIL';
        $user = explode('|', $user);
        $id_user = $user[1];
        $id_admin = $user[2];
        if ($id_admin < 0) {
            $table = 'users';
            $user_id = $id_user;
        }
        else {
            $table = 'admins';
            $user_id = $id_admin;
        }

        if(checkPassword($user[5],$user[6],$table)) {

            $pdo = new PDO('sqlite:'.dirname(__FILE__).'/db/users.sqlite');
            $pdo -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
            try {
                $pdo -> beginTransaction();

                $SQL = 'SELECT * FROM cards WHERE uid=:id';
                $OPT = array(':id' => $user_id);
                $STMT = $pdo -> prepare($SQL);
                $STMT -> execute($OPT);
                $result = "";

                while($value = $STMT->fetch()) {
                    $result .= "|".$value["id"]."£".$value["uid"]."£".$value["title"]."£".$value["recto"]."£".$value["verso"];
                }
            }
            catch (PDOException $e) {
                    $pdo -> rollBack();
            }
        }
    }
    else if($what == "loadBlog") {
        $result = 'FAIL';
        $pdo = new PDO('sqlite:'.dirname(__FILE__).'/db/blog.sqlite');
        $pdo -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
        try {
            $pdo -> beginTransaction();
            $STMT = $pdo->query('SELECT * FROM blog');
            $result = "";

            while($value = $STMT->fetch()) {
                $username = " ";
                $request = getUserWithId($value["uid"],"admins");
                if($request) {
                    $username = $request["NOM"];
                }
                $result .= "|".$value["id"]."£".$value["uid"]."£".$username."£".$value["title"]."£".$value["text"];
            }
        }
        catch (PDOException $e) {
                $pdo -> rollBack();
        }
    }


echo $result;

?>
