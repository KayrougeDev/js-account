## Usage

JS Account is a free and open source project to add account to your website

## Account

You can add account or use the default:

##### Admin:
**Login**: _salut_,
**Password**: _azer_

##### User:
**Login**: _machin_,
**Password**: _azer_


## Installation

Install [PHP](https://www.php.net/)

Put the file in the directory you want on your website, and for every file you created add this on the bottom of the file:

```html
<script src='http://domain.com/jsaccount/assets/js/translate.js'></script>
<script src='http://domain.com/jsaccount/assets/js/session.js'></script>
```

Change JS ligne 13 and 14 to your website data

```javascript
// Default

// The main account page is APPNAME + translateOfUser + username
// For this is example main page title is NameYouWantForAllAccountPage - User - Username
const APPNAME = 'CompteQL',
    // The directory of JS Account
    LOCAL_SESSION_URL = "http://localhost/v2/",

// New
const APPNAME = 'NameYouWantForAllAccountPage', 
    LOCAL_SESSION_URL = "http://domain.com/jsaccount/",
```

(replace domain.com with your domain name and jsaccount with your js-account directory)

Default language is 'fr' you can change the default language at ligne 22

```javascript
// Default
let SESSION = {
    'hostname': '',
    'user': '|-1|-1|||||',
    'userName': '',
    'lang': 'fr',
    'mustLoadData': true,
    'cards' : []
};

// Set english to default language
let SESSION = {
    'hostname': '',
    'user': '|-1|-1|||||',
    'userName': '',
    'lang': 'en',
    'mustLoadData': true,
    'cards' : []
};

// Set spanish to default language
let SESSION = {
    'hostname': '',
    'user': '|-1|-1|||||',
    'userName': '',
    'lang': 'esp',
    'mustLoadData': true,
    'cards' : []
};


```

## Usage

Just go to http://domain.com/jsaccount/ (replace domain.com with your domain name)

## Credit

[Freepik](https://www.flaticon.com/authors/freepik)

[Free Icon](https://www.flaticon.com/free-icon-font/user_3917705)

## License
[GNU](https://www.gnu.org/licenses/)
